/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arbol;

/**
 *
 */
public class ArbolBinario {
    Nodo raiz;
    
    public ArbolBinario(){
        raiz = null;
    }
    
    //metodo para insertar un Nodo en el arbol 
    
    public void agregarNodo(int d, String nom){
        Nodo nuevo = new Nodo(d, nom);
        if(raiz == null){
            raiz = nuevo;
        }else{
            Nodo aux = raiz;
            Nodo padre;
            while(true){
                padre = aux;
                if(d < aux.dato){
                    aux = aux.hijoIzq;
                    if(aux == null){
                        padre.hijoIzq = nuevo;
                        return;
                    }
                }else{
                    aux = aux.hijoDer;
                    if(aux == null){
                        padre.hijoDer = nuevo;
                        return;
                    }
                }
            }
        }
    }
    
    //Metodo para saber si el Arbol esta vacio
    
    public boolean estaVacio(){
        return raiz == null;
    }
    
    // Metodo para recorrer el Arbol InOrden
    
    public void inOrden(Nodo r){
        if(r != null){
            inOrden(r.hijoIzq);
            System.out.println(r.dato + " " + r.nombre);
            inOrden(r.hijoDer);
                   
        }
    }
    
    // Metodo para recorrer el Arbol PreOrden
    
    public void preOrden(Nodo r){
        if(r != null){
            System.out.println(r.dato + " " + r.nombre);
            preOrden(r.hijoIzq);
            preOrden(r.hijoDer);
                   
        }
    }

    // Metodo para recorrer el Arbol PostOrden
    
    public void postOrden(Nodo r){
        if(r != null){            
            postOrden(r.hijoIzq);
            postOrden(r.hijoDer);
            System.out.println(r.dato + " " + r.nombre);      
        }
    }
    
    // Metodo para buscar un nodo en el Arbol
    
    public Nodo buscarNodo(int d){
        Nodo aux = raiz;
        while(aux.dato != d){
            if(d < aux.dato){
                aux = aux.hijoIzq;
            }else{
                aux = aux.hijoDer;
            }
            if(aux == null){
                return null;
            }           
        }
        return aux;
    }
    
    //Metodo para eliminar un Nodo del Arbol
    
    public boolean eliminar(int d){
        Nodo aux = raiz;
        Nodo padre = raiz;
        boolean esHijoIzq = true;
        
        while(aux.dato != d){
            padre = aux;
            if(d < aux.dato){
                esHijoIzq = true;
                aux = aux.hijoIzq;
            }else{
                esHijoIzq = false;
                aux = aux.hijoDer;               
            }
            if(aux == null){
                return false;
            }
        }// fin del while
        if(aux.hijoIzq == null && aux.hijoDer == null){
            if(aux == raiz){
                raiz = null;
            }else if(esHijoIzq){
                padre.hijoIzq = null;
            }else{
                padre.hijoDer = null;
            }
        }else if(aux.hijoDer == null){
            if(aux == raiz){
                raiz = aux.hijoIzq;
            }else if(esHijoIzq){
                padre.hijoIzq = aux.hijoIzq;
            }else{
                padre.hijoDer = aux.hijoIzq;
            }
        }else if(aux.hijoIzq == null){
            if(aux == raiz){
                raiz = aux.hijoDer;
            }else if(esHijoIzq){
                padre.hijoIzq = aux.hijoDer;
            }else{
                padre.hijoDer = aux.hijoDer;
            }
        }else{
            Nodo reemplazo = obtenerNodoReemplazo(aux);
            if(aux == raiz){
                raiz = reemplazo;
            }else if(esHijoIzq){
                padre.hijoIzq = reemplazo;           
            }else{
                padre.hijoDer = reemplazo;
            }
            reemplazo.hijoIzq = aux.hijoIzq;
        }
        return true;   
    }
    
    //Metodo encargado de devolvernos el Nodo reemplazo
    
    public Nodo obtenerNodoReemplazo(Nodo nodoReemp){
        Nodo reemplazarPadre = nodoReemp;
        Nodo reemplazo = nodoReemp;
        Nodo aux = nodoReemp.hijoDer;
        
        while(aux != null){
            reemplazarPadre = reemplazo;
            reemplazo = aux;
            aux = aux.hijoIzq;
        }
        if(reemplazo != nodoReemp.hijoDer){
            reemplazarPadre.hijoIzq = reemplazo.hijoDer;
            reemplazo.hijoDer = nodoReemp.hijoDer;
        }
        System.out.println("El Nodo reemplazo es: " + reemplazo);
        return reemplazo;
    }

}
