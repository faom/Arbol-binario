/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arbol;

/**
 *
 */
public class Nodo {
        int dato;
    String nombre;
    Nodo hijoIzq, hijoDer;
    
    public Nodo(int d, String nom){
        this.dato = d;
        this.nombre = nom;
        this.hijoIzq = null;
        this.hijoDer = null;
    }
    
    public String toString(){
        return nombre + " su dato es: " + dato;
    }
}
