/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arbol;

import java.util.Scanner;
import javax.swing.JOptionPane;

/**
 *
 */
public class Arbol {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int option = 0, elemento;
        String nombre;
        Scanner teclado = new Scanner(System.in);
        ArbolBinario arbol = new ArbolBinario();
        
        do{
           
            
            option = Integer.parseInt(JOptionPane.showInputDialog(null, 
                              "1. Agregar un Nodo \n"
                            + "2. Recorrer el Arbol InOden \n"
                            + "3. Recorrer el Arbol PreOden  \n"
                            + "4. Recorrer el Arbol PostOden  \n"
                            + "5. Buacar un Nodo en el Arbol \n"
                            + "6. Eliminar un Nodo en el Arbol \n"
                            + "7. Salir \n"
                            + "Elige una Opcion...","Arboles Binarios"
                        ,JOptionPane.QUESTION_MESSAGE));
            
            
            switch (option) {
                case 1:
                    elemento = Integer.parseInt(JOptionPane.showInputDialog(null,
                                    "Ingresa el Numero del Nodo...","Agregando Nodo",
                                    JOptionPane.QUESTION_MESSAGE));
                    nombre = JOptionPane.showInputDialog(null, 
                            "Ingrese el Nombre del Nodo","Agregando Nodo"
                            ,JOptionPane.QUESTION_MESSAGE);                   
                    arbol.agregarNodo(elemento, nombre);
                    break;
                case 2:
                    if(!arbol.estaVacio()){
                        arbol.preOrden(arbol.raiz);
                    }else{
                        JOptionPane.showMessageDialog(null, "El Arbol esta vacio","Error"
                            ,JOptionPane.INFORMATION_MESSAGE);
                    }
                    break; 
                case 3:
                    if(!arbol.estaVacio()){
                        System.out.println();
                        arbol.inOrden(arbol.raiz);
                    }else{
                        JOptionPane.showMessageDialog(null, "El Arbol esta vacio","Error"
                            ,JOptionPane.INFORMATION_MESSAGE);
                    }
                    break;
                case 4:
                    if(!arbol.estaVacio()){
                        System.out.println();
                        arbol.postOrden(arbol.raiz);
                    }else{
                        JOptionPane.showMessageDialog(null, "El Arbol esta vacio","Error"
                            ,JOptionPane.INFORMATION_MESSAGE);
                    }
                    break;
                case 5:
                    if(!arbol.estaVacio()){
                        System.out.println();
                        elemento = Integer.parseInt(JOptionPane.showInputDialog(null,
                                    "Ingresa el Numero del Nodo Buscado---","Buscando Nodo",
                                    JOptionPane.QUESTION_MESSAGE));
                        if(arbol.buscarNodo(elemento)==null){
                            JOptionPane.showMessageDialog(null, "El Nodo no se encuentra en el Arbol","Error"
                            ,JOptionPane.INFORMATION_MESSAGE);
                        }else{
                            System.out.println("Nodo encontrado, sus datos son: " + arbol.buscarNodo(elemento));
                        }
                    }else{
                        JOptionPane.showMessageDialog(null, "El Arbol esta vacio","Error"
                            ,JOptionPane.INFORMATION_MESSAGE);
                    }
                    break;
                case 6:
                    if(!arbol.estaVacio()){
                        System.out.println();
                        elemento = Integer.parseInt(JOptionPane.showInputDialog(null,
                                    "Ingresa el Numero del Nodo Eliminar....","Eliminando Nodo",
                                    JOptionPane.QUESTION_MESSAGE));
                        
                        if(arbol.eliminar(elemento) == false){
                            JOptionPane.showMessageDialog(null, "El Nodo no se encuentra en el Arbol","Error"
                            ,JOptionPane.INFORMATION_MESSAGE);
                        }else{
                            JOptionPane.showMessageDialog(null, "El Nodo a sido eliminado en el Arbol","Eliminando"
                            ,JOptionPane.INFORMATION_MESSAGE);
                        }
                    }else{
                        JOptionPane.showMessageDialog(null, "El Arbol esta vacio","Error"
                            ,JOptionPane.INFORMATION_MESSAGE);
                    }
                    break;        
                case 7:
                    JOptionPane.showMessageDialog(null, "Aplicacion Finalizada","Fin"
                            ,JOptionPane.INFORMATION_MESSAGE);
                    break;
                default:
                    JOptionPane.showMessageDialog(null, "Opcion Incorecta..!!","Error"
                            ,JOptionPane.INFORMATION_MESSAGE);
            }
                          
        }while(option != 7);
    }
    
}
